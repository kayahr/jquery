jQuery Maven Artifact
---------------------

The [jQuery JavaScript library][1] packaged as a [Maven][2] artifact suitable 
for the [JavaScript Maven Plugin][3].

Contains the minimized and the uncompressed JavaScript code of the jQuery
library and the [closure compiler externs][4] from Google.

[1]: http://jquery.com "jQuery JavaScript Library"
[2]: http://maven.apache.org/ "Apache Maven"
[3]: https://github.com/kayahr/javascript-maven-plugin "JavaScript Maven Plugin"
[4]: https://github.com/iplabs/closure-compiler/blob/master/contrib/externs/jquery-1.7.js "jQuery Externs"
